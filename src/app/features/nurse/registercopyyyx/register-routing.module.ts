import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register.component';
import {CanDeactivateGuard} from '../../../core/guard/can-deactivate.guard';

const routes: Routes = [
  { 
    path: '', 
    component: RegisterComponent,
    data: {
      breadcrumb: 'ลงทะเบียนผู้ป่วยใน'
    },
    // canDeactivate: [CanDeactivateGuard],
   
   },
  //  { 
  //   path: 'register', 
  //   component: RegisterComponent,
  //   data: {
  //     breadcrumb: 'ลงทะเบียนผู้ป่วยใน'
  //   },
  //   canDeactivate: [CanDeactivateGuard],
   
  //  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
