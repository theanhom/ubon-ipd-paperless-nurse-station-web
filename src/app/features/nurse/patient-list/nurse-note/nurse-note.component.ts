import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { NurseService } from '../../services/nurse.service';
import { DateTime } from 'luxon';
import { NgxSpinnerService, Spinner } from 'ngx-spinner';
import { NotificationService } from '../../../../shared/services/notification.service';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../../shared/services/lib.service';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexMarkers,
  ApexYAxis,
  ApexGrid,
  ApexTitleSubtitle,
  ApexLegend,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
};

@Component({
  selector:'app-nurse-note',
  templateUrl: './nurse-note.component.html',
  styleUrls: ['./nurse-note.component.css'],
})
export class NurseNoteComponent {
  @ViewChild('chart', { static: true }) chart: any;
  public chartOptions: any;
  basicBarChart: any;
  roundnurse: any[] = [''];
  roundnurse1: any[] = ['2','6','10','14','18','22'];
  roundnurse2: any[] = ['2'];
  selectedRoundNurse: any;
  //public
  doctors: any = [];
  doctorname: any;
  //
  //checked = true;
  isVisible = false;
  isVisibleVS = false;
  isVisibleVSEdit = false;
  isVisibleconsult = false;
  isVisibleorder = false;
  isVisiblenurse = false;
  queryParamsData: any;
  size: NzButtonSize = 'default';
  itemPatientInfo: any;
  sessionPatientInfo: any;
  //ken writing
  sessionListnursenote: any;
  listnursenote: any;
  itemActivityInfo: any;
  itemEvaluate: any;
  activity_checked: any = [];
  evaluate_checked: any = [];
  problem_list: any;
  itemNursenote: any;
  getproblem_list: any;
  countrowdata: any;
  itemNursenoteDate: any;
  itemNursenoteTime: any;
  isSaved = false;
  //แสดงข้อมูลด้านบนสุดของแต่ละแท็บ
  topbed: any;
  //Patient info
  address: any;
  admit_id: any;
  age: any;
  an: any;
  blood_group: any;
  cid: any;
  citizenship: any;
  create_by: any;
  create_date: any;
  dob: any;
  fname: any;
  gender: any;
  hn: any;
  id: any;
  inscl: any;
  inscl_name: any;
  insurance_id: any;
  is_active: any;
  is_pregnant: any;
  lname: any;
  married_status: any;
  modify_by: any;
  modify_date: any;
  nationality: any;
  occupation: any;
  phone: any;
  reference_person_address: any;
  reference_person_name: any;
  reference_person_phone: any;
  reference_person_relationship: any;
  religion: any;
  title: any;

  //Nurse V/S info
  body_height: any;
  body_temperature: any;
  body_weight: any;
  chief_complaint: any;
  diatolic_blood_pressure: any;
  eye_score: any;
  movement_score: any;
  oxygen_sat: any;
  past_history: any;
  physical_exam: any;
  present_illness: any;
  pulse_rate: any;
  respiratory_rate: any;
  systolic_blood_pressure: any;
  verbal_score: any;
  waist: any;
  //ken VS input
  rr: any;
  bp: any;
  bw: any;
  bt: any;
  pr: any;
  sp: any;
  dp: any;
  painscore: any;
  oralfluide: any;
  parenterat: any;
  urineout: any;
  emesis: any;
  drainage: any;
  aspiration: any;
  stools: any;
  urine: any;
  medications: any;

  // Nurse Note
  NurseNote: any = [];
  // Doctor Order
  doctorOrder: any = [];
  //Nurse Vital Sign
  NurseVitalSign:any= [];
  //ตัวแปรดึงข้อมูลจากกราฟ
  startdate!:Date;
  nextdate!:Date;
  //แกน x/y
  datax : any=[];
  datax2 : any=[];
  dataxnamex : any=[];
  datay : any=[];
  datetop : any=[];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseService: NurseService,
    private notificationService: NotificationService,
    private spinner: NgxSpinnerService,
    private libService: LibService
  ) {
    this.apexChartLine();
  }
  apexChartLine() {
    this.chartOptions = {
      series: [
        {
          name: 'Temperature',
          data: this.datax,
          type: 'line',
        },
        {
          name: 'Pulse Rate',
          data: this.datax2,
          type: 'line',
          yAxis: 1, // ให้ข้อมูล Pulse Rate ใช้แกน Y ที่ 2
        },
      ],
      chart: {
        height: 350,
        width: '100%',
        type: 'line',
        dropShadow: {
          enabled: true,
          color: '#000',
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2,
        },
        toolbar: {
          show: false,
        },
      },
      colors: ['#ff0000', '#0066ff'],
      dataLabels: {
        enabled: true,
      },
      stroke: {
        curve: 'smooth',
        width: 2,
      },
      title: {
        text: 'Average High and Low Temperature and Pulse Rate',
        align: 'left',
      },
      grid: {
        borderColor: '#e7e7e7',
        row: {
          colors: ['#f3f3f3', 'transparent'],
          opacity: 0.5,
        },
      },
      markers: {
        size: 1,
      },
      xaxis: {
        categories: this.dataxnamex,
        title: {
          text: '',
        },
      },
      yaxis: [
        {
          title: {
            text: 'Temperature',
          },
          annotations: {
            yaxis: [
              {
                y: 30,
                borderColor: "#00E396",
                label: {
                  borderColor: "#00E396",
                  style: {
                    color: "#fff",
                    background: "#00E396"
                  },
                  text: "Y Axis Annotation"
                }
              }
            ],
          },

        },
        {
          title: {
            text: 'Pulse Rate',
          },

        },
      ],
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5,
      },
    };
  }
    async ngOnInit() {
    this.sessionPatientInfo = sessionStorage.getItem('itemPatientInfo');
    //itemPatientInfo = an
    this.itemPatientInfo = JSON.parse(this.sessionPatientInfo);

    //console.log('itemPatientInfo : ', this.itemPatientInfo);
    //ken writing
    await this.listActivity();
    await this.listEvaluate();
    //Patient info
    await this.getNursePatient();
    await this.getDoctor();
    await this.prepareDataChart(this.startdate);
  }


  async getNursenote(id: any) {
    try {
      const respone = await this.nurseService.getNurseNote(id);
      const responseData: any = respone.data;
      let data: any = responseData;
      console.log('Nurse note:', data);
      this.NurseNote = data.data;
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }
  async listActivity() {
    try {
      const respone = await this.nurseService.getActivity();
      this.itemActivityInfo = respone.data.data;
      // console.log(respone);
    } catch (error: any) {}
  }

  async listEvaluate() {
    try {
      const respone = await this.nurseService.getEvaluate();
      this.itemEvaluate = respone.data.data;
      // console.log(respone);
    } catch (error: any) {}
  }
  //แสดง บันทึกทางการพยาบาล nurse note ake
  showModalOrder(): void {
    this.isVisibleorder = true;
  }
  showModalConsult(): void {
    this.isVisibleconsult = true;
  }

  showModal(): void {
    this.isVisible = true;
  }
  showModalnurse(): void {
    this.isVisiblenurse = true;
  }
  handleOknurse(): void {
    // console.log('Button ok clicked!');
    this.saveNurseNote();
    this.isVisiblenurse = false;
  }

  handleCancelnurse(): void {
    // console.log('Button cancel clicked!');
    this.isVisiblenurse = false;
  }

  showModalVS(): void {
    this.isVisibleVS = true;
  }
  //แสดง modal vital sign note
  handleOkVS(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVS = false;
  }

  handleCancelVS(): void {
    this.isVisibleVS = false;
  }
  handleOkorder(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleorder = false;
  }

  handleCancelorder(): void {

    this.isVisibleorder = false;
  }
  showModalVSEdit(): void {
    this.isVisibleVSEdit = true;
  }
  //แสดง modal vital sign note
  handleOkVSEdit(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVSEdit = false;
  }

  handleCancelVSEdit(): void {
    // console.log('Button cancel clicked!');
    this.isVisibleVSEdit = false;
  }

  async getActivity(i: any, e: any) {
    console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.activity_checked.push(i);
    } else {
      this.removeItemArray(this.activity_checked, i);
    }
    console.log(this.activity_checked);
  }

  async getEvaluate(i: any, e: any) {
    console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.evaluate_checked.push(i);
    } else {
      this.removeItemArray(this.evaluate_checked, i);
    }
    console.log(this.evaluate_checked);
  }

  removeItemArray(array: any, item: any) {
    for (var i in array) {
      if (array[i] == item) {
        array.splice(i, 1);
        break;
      }
    }
  }

  async saveNurseNote() {
    let date: Date = new Date();
    // console.log(date);

    let data: any = {
      nurse_note_date: '2023-08-06',
      nurse_note_time: '12:22:00',
      problem_list: [this.problem_list],
      activity: this.activity_checked,
      evaluate: this.evaluate_checked,
      admit_id: this.itemPatientInfo.id,
      create_date: '2023-08-06',
      create_by: sessionStorage.getItem('userID'),
      modify_date: '',
      modify_by: sessionStorage.getItem('userID'),
      is_active: true,
      item_used: [{}],
    };
    // console.log(data);
    try {
      if (this.activity_checked.length || this.evaluate_checked.length) {
        const respone = await this.nurseService.saveNurseNote(data);
        // console.log(respone);
      }
    } catch (error: any) {}
  }
  async saveNurseVitalsign() {
    let date: Date = new Date();
    let isValidated: boolean = true;
    console.log('Vigtalsign วันที่: ' + date);

    let data: any = {
      admit_id: this.getadmit_id,
      vital_sign_date: '2023-01-01',
      vital_sign_time: '11:00:00',

      round_time: this.selectedRoundNurse,

      respiratory_rate: this.rr,
      systolic_blood_pressure: this.sp,
      diatolic_blood_pressure: this.dp,
      body_weight: this.bw,
      body_temperature: this.bt,
      pulse_rate: this.pr,
      pain_score: this.painscore,

      intake_oral_fluid: this.oralfluide,
      intake_penterate: this.parenterat,

      outtake_urine: this.urineout,
      outtake_emesis: this.emesis,
      outtake_drainage: this.drainage,
      outtake_aspiration: this.aspiration,

      stools: this.stools,
      urine: this.urine,
      intake_medicine: this.medications,

      outtake_lochia: '99',
      oxygen_sat: '99',
      create_at: '',
      create_by: this.getadmit_id,
      modify_at: '',
      modify_by: this.getadmit_id,

    };
    console.log("Vigtalsign data: "+data);
    let error_message : string = '';
    if(this.selectedRoundNurse == '' || this.selectedRoundNurse == null){
      error_message += 'ยังไม่ได้บันทึกรอบที่วัด<br>';
      isValidated = false;
    }
    if(this.bt == '' || this.bt == null){
      error_message += 'ยังไม่ได้บันทึกอุณหภูมิร่างการ<br>';
      isValidated = false;
    }
    if(this.pr == '' || this.pr == null){
      error_message += 'ยังไม่ได้บันทึกอัตราการเต้นของหัวใจ<br>';
      isValidated = false;
    }
    if(this.sp == '' || this.sp == null){
      error_message += 'ยังไม่ได้บันทึกความดันโลหิด Systolic<br>';
      isValidated = false;
    }
    if(this.dp == '' || this.dp == null){
      error_message += 'ยังไม่ได้บันทึกความดันโลหิด Diastolic';
      isValidated = false;
    }
    if(isValidated){
      console.log("Vitalsign ดึงข้อมูล: "+data);
      try {
        const response = await this.nurseService.saveNurseVitalSign(data);
        console.log("ken"+response);
        if (response.status === 200) {
          this.hideSpinner();
          this.notificationService.notificationSuccess('คำชี้แจ้ง', 'บันทึกสำเร็จ..', 'top');
          this.isSaved = true;
          setTimeout(() => {
            this.navigateNurse();
          }, 2000);

        }

      } catch (error) {
        console.log(error);
        this.hideSpinner();
        this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
      }
    } else {
      await this.notificationService.notificationWaring('ข้อผิดพลาด', error_message, 'top');
      this.showModalVS();
    }

  }
  async navigateNurse() {
    console.log('บันทึกสำเร็จ');
    this.router.navigate(['/nurse/patient-list/nurse-note']);
  }
  getadmit_id: any;
  async getNursePatient() {
    try {
      const response = await this.nurseService.getNursePatient(
        this.itemPatientInfo
      );
      this.getadmit_id = response.data.data[0].admit_id;
      this.address = response.data.data[0].address;
      this.admit_id = response.data.data[0].admit_id;
      this.age = response.data.data[0].age;
      this.an = response.data.data[0].an;
      this.blood_group = response.data.data[0].blood_group;
      this.cid = response.data.data[0].cid;
      this.citizenship = response.data.data[0].citizenship;
      this.create_by = response.data.data[0].create_by;
      this.create_date = response.data.data[0].create_date;
      this.dob = response.data.data[0].dob;
      this.fname = response.data.data[0].fname;
      this.gender = response.data.data[0].gender;
      this.hn = response.data.data[0].hn;
      this.id = response.data.data[0].id;
      this.inscl = response.data.data[0].inscl;
      this.inscl_name = response.data.data[0].inscl_name;
      this.insurance_id = response.data.data[0].insurance_id;
      this.is_active = response.data.data[0].is_active;
      this.is_pregnant = response.data.data[0].is_pregnant;
      this.lname = response.data.data[0].lname;
      this.married_status = response.data.data[0].married_status;
      this.modify_by = response.data.data[0].modify_by;
      this.modify_date = response.data.data[0].modify_date;
      this.nationality = response.data.data[0].nationality;
      this.occupation = response.data.data[0].occupation;
      this.phone = response.data.data[0].phone;
      this.reference_person_address =
        response.data.data[0].reference_person_address;
      this.reference_person_name = response.data.data[0].reference_person_name;
      this.reference_person_phone =
        response.data.data[0].reference_person_phone;
      this.reference_person_relationship =
        response.data.data[0].reference_person_relationship;
      this.religion = response.data.data[0].religion;
      this.title = response.data.data[0].title;

      //ken ดึงข้อมูล เตียงมาแสดง
      this.getPatientdataadmit(this.admit_id);

      //ดึงข้อมูลมาใส่ตัวแปล

      //tab Info
      await this.getNurseInfo();
      //tab Note
      await this.getNursenote(this.admit_id);
      //tab V/S
      await this.getNurseVitalSign(this.admit_id);
      //tab Doctor Order
      await this.getOrder(this.admit_id);
      // console.log('NuPatient',response);
    } catch (error) {}
  }
  async getPatientdataadmit(id: any) {
    try {
      const response = await this.nurseService.getDatabedanddoc(id);
      for (let i of this.doctors) {
        // console.log("loop docrot iiii",i);
        if (i.user_id == response.data.data[0].doctor_id) {
          // console.log("loop docrot",i);
          this.doctorname = i.title + i.fname + ' ' + i.lname;
          // console.log("ชื่อหมด:",this.doctorname);
        } else {
          console.log('หาหมอไม่เจอ');
        }
      }

      this.topbed = response.data.data[0].bed_number;
      // console.log('getDatabedanddoc:',response);
    } catch (error) {}
  }
  //หาหมอ
  async getDoctor() {
    console.log('getDoctor');
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      console.log('getDoctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }

  async getNurseInfo() {
    try {
      const response = await this.nurseService.getNurseinfo(this.getadmit_id);
      console.log('admit_id:', response);
      this.body_height = response.data.data[0].body_height;
      this.body_temperature = response.data.data[0].body_temperature;
      this.body_weight = response.data.data[0].body_weight;
      this.chief_complaint = response.data.data[0].chief_complaint;
      this.diatolic_blood_pressure =
        response.data.data[0].diatolic_blood_pressure;
      this.eye_score = response.data.data[0].eye_score;
      this.movement_score = response.data.data[0].movement_score;
      this.oxygen_sat = response.data.data[0].oxygen_sat;
      this.past_history = response.data.data[0].past_history;
      this.physical_exam = response.data.data[0].physical_exam;
      this.present_illness = response.data.data[0].present_illness;
      this.pulse_rate = response.data.data[0].pulse_rate;
      this.respiratory_rate = response.data.data[0].respiratory_rate;
      this.systolic_blood_pressure =
        response.data.data[0].systolic_blood_pressure;
      this.verbal_score = response.data.data[0].verbal_score;
      this.waist = response.data.data[0].waist;
    } catch (error) {}
  }

//สร้างข้อมูล
async prepareDataChart(date_start:Date){
  let x :any = [];
  let bt : any=[];
  let bt2 : any=[];
  let dateweek:any=[];
  let vs = this.NurseVitalSign[0].data_vital_sign;
  for(let v of vs) {
    let check : any = v.vital_sign_date.substring(0,10) + 'R' + v.round_time;
    v.checked = check;
  }
   console.log('vs',vs);


  for(let i = 0; i < 7 ; i++) {
    let record_date = new Date(date_start);
    record_date.setDate(record_date.getDate()+i);
    let next_day = record_date;
    next_day.setDate(next_day.getDate()+1);
    let data : any;
    let dateloopweek : any;
    for(let j = 0 ; j < 6 ; j++){
      let r_date : string = record_date.toISOString().substring(0,10);
      let order_round = (((j+1)*4)-2);
      data = r_date + 'R' + order_round;
      dateloopweek = r_date;
      x.push(data);
      let _vs :any = vs.find((v:any) => v.checked == data);
      if(_vs == undefined) {
        bt.push(0);
        bt2.push(0);
      } else {
        bt.push(_vs.body_temperature)
        bt2.push(_vs.pulse_rate)
      }
    }
    this.nextdate = next_day;
    dateweek.push(dateloopweek);
    this.datetop = dateweek;

  }
  this.datax = bt;
  this.datax2 = bt2;
  this.dataxnamex = x;
  console.log("datax",bt);
  console.log("datax2",bt2);
  console.log("datax3",x);
  console.log("dateweek",dateweek);

}
  async getNurseVitalSign(id: any) {
    try {
      const respone = await this.nurseService.getNursevital_sign(id);
      const responseData: any = respone.data;
      let data : any = responseData;
      console.log('Nurse vital sign:',data);
      this.NurseVitalSign = data.data_admit;
      this.startdate = data.data_admit[0].admit_date;
      console.log('v/s:',this.NurseVitalSign[0].data_vital_sign[0].body_temperature);
    } catch (error) {}
  }

  async getOrder(id: any) {
    try {
      const respone = await this.nurseService.getorder(id);
      const responseData: any = respone.data;
      let data: any = responseData.getAdmitID;
      console.log('doctor order:', data);
      this.doctorOrder = data;
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError(
        'คำชี้แจ้ง',
        'พบข้อผิดพลาด..' + error,
        'top'
      );
    }
  }


  //ken
  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
}
