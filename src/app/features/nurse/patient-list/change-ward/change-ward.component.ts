import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { DateTime } from 'luxon';
import { NurseService } from '../../services/nurse.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

import { AxiosResponse } from 'axios';
import { LibService } from 'src/app/shared/services/lib.service';
import * as _ from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgxSpinnerService, Spinner } from "ngx-spinner";
import { NotificationService } from '../../../../shared/services/notification.service';
import { CanComponentDeactivate } from '../../../../core/guard/can-deactivate.guard'



@Component({
  selector: 'app-change-ward',
  templateUrl: './change-ward.component.html',
  styleUrls: ['./change-ward.component.css']
})
export class ChangeWardComponent implements OnInit, CanComponentDeactivate {
  // confirmModal?: NzModalRef;
  confirmModal?: NzModalRef<any, any> | undefined;
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];

  dataSetTreatement: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  admitId: any;
  wardId: any;
  doctorId: any;
  wards: any = [];
  beds: any = [];
  doctors: any = [];
  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  //private routeSub: any;  // subscription to route observer
  wardTags: any[] = [];
  bedTags: any[] = [];
  doctor_name :any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseService: NurseService,
    private spinner: NgxSpinnerService,
    private nzMessageService: NzMessageService,
    private modal: NzModalService,
    private libService: LibService,

    private notificationService: NotificationService


  ) {


    this.userId = sessionStorage.getItem('userID');
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    this.admitId = this.queryParamsData.admit_id;
    console.log(this.queryParamsData);


  }

  async ngOnInit() {
    await this.getWard();
    await this.getDoctor();
    await this.assignCollapseWard();
    await this.getAdmit();
  }

  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  async canDeactivate() {
    if (!this.isSaved) {
      const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');
      if (confirm) {
        this.isSaved = true;
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  showConfirmModal(title: string, content: string): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: title,
        nzContent: content,
        nzOnOk: () => {
          resolve(true);
        },
        nzOnCancel: () => {
          resolve(false);
        }
      });
    });
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }

  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }

  //////////////////////

  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      console.log('Ward : ', this.wards);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  async getDoctor() {
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      if (!_.isEmpty(data)) {
        // this.doctorId = id;
      }
      console.log('Doctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  async assignCollapseWard() {
    this.panelsWard = [
      {
        active: true,
        name: 'เลือกตึก',
        disabled: false
      },
    ];
  }

  assignCollapseBed() {
    this.panelsBed = [
      {
        active: true,
        name: 'เลือกเตียง',
        disabled: false
      },
    ];
  }

  async setWard(data: any) {
    console.log(data);

    this.wardId = data.id;
    let wardName = data.name;
    this.departmentId = data.department_id;
    this.panelsWard = [
      {
        active: false,
        name: 'เลือกตึก : ' + wardName,
        disabled: false
      },
    ];
    console.log('wardId_now : ', this.wardId);
    this.handleInputConfirm(wardName);
    this.bedId = '';
    await this.getBed(this.wardId);
  }

  async getBed(wardId: any) {
    try {
      console.log('wardsId : ', wardId);
      const response: AxiosResponse = await this.libService.getBed(wardId);
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.beds = data;
      this.assignCollapseBed();
      console.log('Beds : ', this.beds);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');


    }
  }

  async setBed(data: any) {
    console.log('dataBeds : ', data);

    this.bedId = data.bed_id;
    let bedName = data.name;
    this.panelsBed = [
      {
        active: false,
        name: 'เลือกเตียง : ' + bedName,
        disabled: false
      },
    ];
    this.handleInputConfirmBed(bedName);
    console.log('bedId : ', this.bedId);
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  async saveRegister() {
    this.isSaved = true;
    this.spinner.show();
    let info: any = {
      "patient": [
        {
          "an": this.queryParamsData.an,
          "hn": this.queryParamsData.hn,
          "cid": this.queryParamsData.cid,
          "title": this.queryParamsData.title,
          "fname": this.queryParamsData.fname,
          "lname": this.queryParamsData.lname,
          "gender": this.queryParamsData.gender,
          "dob": this.queryParamsData.dob,
          "age": this.queryParamsData.age,
          "nationality": this.queryParamsData.nationality,
          "citizenship": this.queryParamsData.citizenship,
          "religion": this.queryParamsData.religion,
          "married_status": this.queryParamsData.married_status,
          "occupation": this.queryParamsData.occupation,
          "blood_group": this.queryParamsData.blood_group,
          "inscl": this.queryParamsData.inscl,
          "inscl_name": this.queryParamsData.inscl_name,
          "address": this.queryParamsData.address,
          "phone": this.queryParamsData.phone,
          "is_pregnant": this.queryParamsData.is_pregnant,
          "reference_person_name": this.queryParamsData.reference_person_name,
          "reference_person_phone": this.queryParamsData.reference_person_phone,
          "reference_person_address": this.queryParamsData.reference_person_address,
          "reference_person_relationship": this.queryParamsData.reference_person_relationship
        }
      ],
      "review": this.dataSetReview,
      "treatment": this.dataSetTreatement,
      "admit": this.dataSetAdmit,
      "bed": [
        {
          "bed_id": this.bedId,
          "status": "Used"
        }
      ],
      "ward": [
        {
          "ward_id": this.wardId,
        }
      ],
      "doctor": [
        { "user_id": this.userId }
      ],
      "department": [
        { "department_id": this.departmentId }
      ]
    }
    console.log(info);
    try {
      const response = await this.nurseService.saveRegister(info);
      console.log(response);
      if (response.status === 200) {
        this.hideSpinner();
        this.notificationService.notificationSuccess('คำชี้แจ้ง', 'บันทึกสำเร็จ..', 'top');
        this.isSaved = true;
        setTimeout(() => {
          this.navigatePatientList();
        }, 2000);

      }

    } catch (error) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
    }

  }




  handleOk(): void {
    this.isVisible = false;
    this.saveRegister();

  }

  handleCancel(): void {
    console.log('Cancel Save');
  }



  async navigatePatientList() {

    this.router.navigate(['/nurse/patient-list']);

  }

  /////////////////get data  connter here/////////////////
  handleInputConfirm(tag: any): void {

    this.wardTags = [tag];

  }
  handleClose(removedTag: {}): void {
    this.wardTags = this.wardTags.filter(tag => tag !== removedTag);
  }

  sliceTagName(tag: string): string {
    const isLongTag = tag.length > 20;
    return isLongTag ? `${tag.slice(0, 20)}...` : tag;
  }

  ///////////////////////////////////
  handleInputConfirmBed(tag: any): void {
    this.bedTags = [tag];

  }
  handleCloseBed(removedTag: {}): void {
    this.bedTags = this.bedTags.filter(tag => tag !== removedTag);
  }

  sliceTagNamebed(tag: string): string {
    const isLongTag = tag.length > 20;
    return isLongTag ? `${tag.slice(0, 20)}...` : tag;
  }


  async getAdmit() {
    console.log('getAdmit:');
    try {
      const response = await this.nurseService.getAdmit(this.queryParamsData.admit_id)
      console.log(response);
      const data = await response.data;
      this.dataSetAdmit = await data.data;
      console.log('getAdmit : ', this.dataSetAdmit);
      for (let d of this.doctors){
      if(d.id=this.dataSetAdmit[0].doctor_id){
        this.doctor_name = d.title+d.fname + ' ' + d.lname;
      }
      }
      let findWard = _.find(this.wards, function (o) { return o.id == data.data[0].ward_id; }) ?? { name: 'ยังไม่ได้ระบุ' };
      let dataWard: any = {
        id: data.data[0].ward_id,
        name: findWard.name,
      }
      await this.setWard(dataWard);

      let findBed = _.find(this.beds, function (o) { return o.bed_id == data.data[0].bed_id; }) ?? { name: 'ยังไม่ได้ระบุ' };
      console.log(findBed);
      let dataBed: any = {
        bed_id: data.data[0].bed_id,
        name: findBed.name,
      }
      await this.setBed(dataBed);

      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async changeWard() {
    console.log('changeWard');
    let info: object = {
      ward_id: this.wardId,
      old_id: this.dataSetAdmit[0].ward_id 
    }
    console.log(info);

    try {
      const response = await this.nurseService.changeWard(info, this.admitId)
      console.log(response);
      if (response.status === 200) {
        await this.changeBed();
        this.hideSpinner();
        // this.notificationService.notificationSuccess('คำชี้แจ้ง', 'บันทึกสำเร็จ..', 'top');
        // this.isSaved = true;
        // setTimeout(() => {
        //   // this.navigatePatientList();
        //   console.log('ok');
        // }, 2000);

      }

    } catch (error) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
    }
  }
  async changeBed() {
    console.log('changeBed');
    console.log(this.dataSetAdmit[0].bed_id);
    let info: object = {
      bed_id: this.bedId,
      old_id: this.dataSetAdmit[0].bed_id  ?? "c28c579a-00e4-45a0-921a-9dc1358b4067"
    }
    console.log(info);

    try {
      const response = await this.nurseService.changeBed(info, this.admitId)
      console.log(response);
      if (response.status === 200) {
        this.hideSpinner();
        this.notificationService.notificationSuccess('คำชี้แจ้ง', 'บันทึกสำเร็จ..', 'top');
        this.isSaved = true;
        setTimeout(() => {
          console.log('changeBed ok');          
          // this.navigatePatientList();         
        }, 2000);

      } else {
        console.log('No changeBed ok');
        this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + response.status, 'top');
      }

    } catch (error) {
      console.log(error);
      this.hideSpinner();
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');
    }
  }


}

